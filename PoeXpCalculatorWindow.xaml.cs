﻿using System;
using System.Windows;

namespace poe_xp_calculator {
    public partial class PoeXpCalculatorWindow {
        public PoeXpCalculatorWindow() {
            InitializeComponent();
        }

        private static int ParseStringToPositiveInteger(string field, string fieldName) {
            int output;
            if (!Int32.TryParse(field, out output) || output <= 0) {
                throw new ArgumentException(string.Format("{0} is not a positive integer.", fieldName));
            }
            return output;
        }

        private void BtnCalculate_Click(object sender, RoutedEventArgs e) {
            int playerLevel, zoneLevel;
            try {
                playerLevel = ParseStringToPositiveInteger(TxfPlayerLevel.Text, "Player level");
                zoneLevel = ParseStringToPositiveInteger(TxfZoneLevel.Text, "Zone level");
            }
            catch (ArgumentException exception) {
                MessageBox.Show(this, exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var effectiveDifference = Math.Max(playerLevel, zoneLevel) - Math.Min(playerLevel, zoneLevel);
            var result = Math.Pow(((playerLevel + 5) / (playerLevel + 5 + Math.Pow(effectiveDifference, 2.5))), 1.5);
            result = Math.Round(result * 100, 2);
            TxfResult.Text = string.Format("{0} %", result);
        }
    }
}
